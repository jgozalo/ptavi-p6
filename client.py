#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

# Cliente UDP simple.

# Modo de ejecución del cliente

try:
    METHOD = sys.argv[1].upper()
    RECEIVER = sys.argv[2].split(":")
    RECEIVER_NAME = RECEIVER[0].split("@")[0]
    SERVER = RECEIVER[0].split("@")[1]
    PORT = int(RECEIVER[1])
except (IndexError, ValueError):
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")


# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((SERVER, PORT))

    # Contenido que vamos a enviar
    LINE = METHOD + " sip:" + RECEIVER_NAME + " SIP/2.0\r\n\r\n"
    print("Enviando: " + LINE)
    my_socket.send(bytes(LINE, 'utf-8'))
    data = my_socket.recv(1024)

    print('Recibido -- ', data.decode('utf-8'))
    ANSWERS = "SIP/2.0 100 Trying\r\n\r\n" \
              "SIP/2.0 180 Ringing\r\n\r\n" \
              "SIP/2.0 200 OK\r\n\r\n"
    """and data.decode('utf-8') == ANSWERS:"""
    if METHOD == "INVITE":
        LINE = "ACK" + " sip:" + RECEIVER_NAME + " SIP/2.0\r\n\r\n"
        print()
        print("Enviando: " + LINE)
        my_socket.send(bytes(LINE, 'utf-8'))
    print("Terminando socket...")

print("Fin.")
