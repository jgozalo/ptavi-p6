#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import simplertp
import random


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)

        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            if not line:
                break
            DECODED_LINE = line.decode('utf-8')
            print("El cliente nos manda " + DECODED_LINE)

            sMETHOD = DECODED_LINE.split(" ")[0]
            if sMETHOD == "INVITE":
                body = "\r\n\r\nv=0\r\no=batmangotham 127.0.0.2\r\n" \
                       "s=misesion\r\nt=0\r\nm=audio 5060 RTP\r\n"
                header = "SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 Ringing" \
                         "\r\n\r\n SIP/2.0 200 OK\r\nContent-Length:"\
                         + str(len(body))
                sANSWERS = header + body
                self.wfile.write(bytes(sANSWERS, 'utf-8'))

            elif sMETHOD == "ACK":
                ALEAT = random.randint(1, 11111)
                cabeceraRTP = simplertp.RtpHeader()
                cabeceraRTP.set_header(version=2, pad_flag=0, ext_flag=0, cc=0,
                                       marker=0, payload_type=14, ssrc=ALEAT)
                audio = simplertp.RtpPayloadMp3(AUDIO_FILE)
                simplertp.send_rtp_packet(
                    cabeceraRTP, audio, "127.0.0.1", 23032)

            elif sMETHOD == "BYE":
                sANSWERS = "SIP/2.0 200 OK\r\n\r\n"
                self.wfile.write(bytes(sANSWERS, 'utf-8'))

            elif sMETHOD != "BYE" and sMETHOD != "INVITE" and sMETHOD != "ACK":
                sANSWERS = "SIP/2.0 405 Method Not Allowed"
                self.wfile.write(bytes(sANSWERS, 'utf-8'))
            else:
                sANSWERS = "SIP/2.0 400 Bad Request"
                self.wfile.write(bytes(sANSWERS, 'utf-8'))


if __name__ == "__main__":
    try:
        IP = sys.argv[1]
        PORT = int(sys.argv[2])
        AUDIO_FILE = sys.argv[3]
    except (IndexError, ValueError):
        sys.exit("Usage: python3 server.py IP port audio_file")
    # Creamos servidor de eco y escuchamos
    serv = socketserver.UDPServer(('', PORT), EchoHandler)
    try:
        print("Listening...")
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
